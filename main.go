package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/vudoan2016/portfolio/analysis"
	"github.com/vudoan2016/portfolio/input"
	"github.com/vudoan2016/portfolio/models"
	"github.com/vudoan2016/portfolio/output"
	"github.com/vudoan2016/portfolio/util"
)

func main() {
	loc, err := time.LoadLocation("EST")
	if err != nil {
		log.Fatal(err)
	}

	// Initialize the logger
	logger, err := os.OpenFile("info.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}

	util.LoggerInit(logger)
	log.SetOutput(logger)
	defer logger.Close()

	var file = "portfolio.json"
	if len(os.Args) < 2 {
		file = find(file)
		if len(file) == 0 {
			fmt.Println(file, "not found")
			os.Exit(1)
		}
	} else {
		file = os.Args[1]
	}

	// Initialize the database
	db := models.ConnectDataBase()
	defer db.Close()

	// Create Redis client
	redisClient := redis.NewClient(&redis.Options{
		Addr:     getEnv("REDIS_URL", "localhost:6379"),
		Password: getEnv("REDIS_PASSWORD", ""),
		DB:       0,
	})

	_, err = redisClient.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}

	// Initialize the router
	router := gin.Default()
	router.LoadHTMLGlob("output/*.html")

	router.Use(func(ctx *gin.Context) {
		ctx.Set("db", db)
		ctx.Next()
	})

	// Load portfolio data
	portfolio := input.Get(file)

	profChannel := make(chan map[string]models.Company)
	profSignal := analysis.GetProfiles(portfolio, db, profChannel)

	// Poll stock prices & perform simple analysis
	go analysis.Run(portfolio, profChannel, profSignal, redisClient, loc)

	// Ready to serve
	router.GET("/", output.Respond)
	router.GET("/:id/:type", output.RespondEquity)
	router.Run(":8080")
}

// Find file in current directory and level-1 subdirectories
func find(name string) string {
	files, err := ioutil.ReadDir(".")
	if err == nil {
		for _, f := range files {
			if f.IsDir() {
				pattern := f.Name() + "/" + name + "*"
				matches, err := filepath.Glob(pattern)
				if err == nil && len(matches) > 0 {
					return "./" + strings.Replace(matches[0], "\\", "/", 1)
				}
			} else if strings.Contains(f.Name(), name) {
				return f.Name()
			}
		}
	}
	return ""
}

func getEnv(key, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}
