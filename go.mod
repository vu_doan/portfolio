module github.com/vudoan2016/portfolio

go 1.13

require (
	github.com/gin-gonic/gin v1.7.4
	github.com/go-delve/delve v1.7.3 // indirect
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.11.4 // indirect
	github.com/jinzhu/gorm v1.9.12
	github.com/markcheno/go-quote v0.0.0-20200412134556-15afc0fbe141 // indirect
	github.com/mattn/go-sqlite3 v2.0.1+incompatible
	github.com/piquette/finance-go v1.0.0
	github.com/shopspring/decimal v0.0.0-20200227202807-02e2044944cc // indirect
	github.com/tealeg/xlsx v1.0.5 // indirect
)
